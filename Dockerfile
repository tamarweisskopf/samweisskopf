FROM faucet/python3
COPY app.py app.py
COPY requirment.txt requirment.txt
RUN pip3 install -r requirment.txt
CMD [ "python", "app.py" ]
