from flask import Flask, jsonify, request, g, Response, send_from_directory
import redis

app = Flask(__name__)
my_redis = redis.Redis()


@app.route('/')
def whoami():
    ip = request.remote_addr
    if my_redis.exists(ip):
        acces_counter = int(my_redis.get(f'{ip}_counter'))
    else:
        my_redis.setex(ip, 60, 0)
        acces_counter = 0
    if acces_counter >= 10:
        return jsonify({'error_massage': 'too many tries for now, try again later'}), 429
    else:
        my_redis.set(f'{ip}_counter', acces_counter + 1)

    return jsonify({'ip': ip})


if __name__ == '__main__':
    app.run('localhost', port=8000)

