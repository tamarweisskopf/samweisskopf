# Instructions for running this app #

### Build the app using the Dockerfile (you will be find it in the repo)
docker build -t my_app .

### Pull redis image
docker pull redis

### Create and run the dockers based on the two images you just created.
docker create --name my_redis redis
docker create --name my_app my_app

### You should configure the conteiner network (the best configuration deppends on your env) and link the redis container to your app conteiner
### Or just use the host network for both (not recomended)
docker run --name my-redis --network host -d my_redis
docker run --name my-app --network host -d my_app

enjoy it :)
